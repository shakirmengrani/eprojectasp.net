﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace webAppTest.Models
{
    public abstract class DB
    {
        private SqlConnection DB_CONN = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ToString());
        private SqlCommand DB_CMD;
        private SqlDataAdapter DB_ADAP;
        private DataSet DB_DSET;
        protected Dictionary<string, string> param;

        
        public DB() {
            
        }

        protected DataSet getData(string Sql) {
            DB_CMD = new SqlCommand(Sql,DB_CONN);
            if (param != null) {
                foreach (var item in param)
                {
                    DB_CMD.Parameters.Add(new SqlParameter(item.Key, item.Value));
                }
            }
            DB_ADAP = new SqlDataAdapter(DB_CMD);
            DB_DSET = new DataSet();
            DB_ADAP.Fill(DB_DSET,"tbl");
            return DB_DSET;
        }

        protected string setData(string Sql) {
            string err = "";
            try {
                DB_CONN.Open();
                DB_CMD = new SqlCommand(Sql, DB_CONN);
                if (param != null)
                {
                    foreach (var item in param)
                    {
                        DB_CMD.Parameters.Add(new SqlParameter(item.Key, item.Value));
                    }
                }
                DB_CMD.ExecuteNonQuery();
                DB_CONN.Close();
                err = "executed";
            }
            catch (SqlException ex) {
                if (DB_CONN.State == ConnectionState.Open) {
                    DB_CONN.Close();
                }
                err = ex.Message;
            }
            return err;
        }



    }
}