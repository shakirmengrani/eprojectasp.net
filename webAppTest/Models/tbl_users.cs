﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webAppTest.Models
{
    public class tbl_users : DB
    {
        public int ID;
        public string FirstName;
        public string LastName;
        public string Email;
        public string Phone;
        public string UserName;
        public string Password;
        public int RoleId;

        public System.Data.DataSet Read(int ID = 0) {
            string msntx = "";
            if (ID > 0)
            {
                msntx = "Where ID=@ID";
                param = new Dictionary<string, string>();
                param.Add("@ID", ID.ToString());
            }

            return getData("Select * from tbl_users " + msntx);
        }

        public string Create()
        {
            string sql = "Insert Into tbl_users (FirstName,LastName,Email,Phone,UserName,Passwd,RID) values(@FirstName,@LastName,@Email,@Phone,@UserName,@Password,@RID)";
            param = new Dictionary<string, string>()
            {
                { "@FirstName",this.FirstName },
                { "@LastName",this.LastName },
                { "@Email",this.Email },
                { "@UserName",this.UserName },
                { "@Password",this.Password},
                { "@Phone",this.Phone },
                { "@RID",this.RoleId.ToString() }
            };
            return this.setData(sql);
        }

        public string Update() {
            string sql = "Update tbl_users set FirstName=@FirstName,LastName=@LastName,Email=@Email,Phone=@Phone,UserName=@UserName,Passwd=@Password,RID=@RID Where ID=@ID";
            param = new Dictionary<string, string>()
            {
                { "@FirstName",this.FirstName },
                { "@LastName",this.LastName },
                { "@Email",this.Email },
                { "@UserName",this.UserName },
                { "@Password",this.Password},
                { "@Phone",this.Phone },
                { "@RID",this.RoleId.ToString() },
                { "@ID", this.ID.ToString() }
            };
            return this.setData(sql);
        }

        public string Delete()
        {
            string sql = "Delete From tbl_users Where ID=@ID";
            param = new Dictionary<string, string>()
            {
                { "@ID", this.ID.ToString() }
            };
            return this.setData(sql);
        }
    }
}