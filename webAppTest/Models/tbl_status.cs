﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webAppTest.Models
{
    public class tbl_status : DB
    {
        public int ID;
        public string Name;

        public System.Data.DataSet Read(int ID = 0)
        {
            string msntx = "";
            if (ID > 0)
            {
                msntx = "Where ID=@ID";
                param = new Dictionary<string, string>();
                param.Add("@ID", ID.ToString());
            }

            return getData("Select * from tbl_status " + msntx);
        }

        public string Create()
        {
            string sql = "Insert Into tbl_status (Name) values(@Name)";
            param = new Dictionary<string, string>()
            {
                { "@Name",this.Name }
            };
            return this.setData(sql);
        }

        public string Update()
        {
            string sql = "Update tbl_status set Name=@Name Where ID=@ID";
            param = new Dictionary<string, string>()
            {
                { "@Name",this.Name },
                { "@ID", this.ID.ToString() }
            };
            return this.setData(sql);
        }

        public string Delete()
        {
            string sql = "Delete From tbl_status Where ID=@ID";
            param = new Dictionary<string, string>()
            {
                { "@ID", this.ID.ToString() }
            };
            return this.setData(sql);
        }
    }
}