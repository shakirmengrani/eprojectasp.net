﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webAppTest.Controllers
{
    public class CategoryController : Controller
    {
        private Models.tbl_category category = new Models.tbl_category();
        // GET: category
        public ActionResult Index()
        {
            ViewBag.Data = category.Read().Tables[0];
            return View();
        }

        // GET: category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: category/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                category.Name = collection["txt_Name"];
                ViewBag.err = category.Create();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: category/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Data = category.Read(id).Tables[0].Rows;
            return View();
        }

        // POST: category/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                category.Name = collection["txt_Name"];
                category.ID = id;
                ViewBag.err = category.Update();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: category/Delete/5
        public ActionResult Delete(int id)
        {

            return View();
        }

        // POST: category/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                category.ID = id;
                category.Delete();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
