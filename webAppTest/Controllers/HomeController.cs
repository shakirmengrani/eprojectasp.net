﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webAppTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var my = new webAppTest.Models.tbl_users();
           // ViewBag.Name =  Library.DataHandler.DBValFilter<string>(my.Read().Tables[0].Rows[0]["Name"]);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}