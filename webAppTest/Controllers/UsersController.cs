﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webAppTest.Controllers
{
    public class UsersController : Controller
    {
        private Models.tbl_users users = new Models.tbl_users();
        // GET: Users
        public ActionResult Index()
        {
            ViewBag.Data = users.Read().Tables[0];  
            return View();
        }

        // GET: Users/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            List<SelectListItem> RoleLst = new List<SelectListItem>();
            RoleLst.Add(new SelectListItem { Text = "Client", Value = "2" });
            ViewBag.Role = RoleLst;
            return View();
        }

        // POST: Users/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                users.FirstName = collection["txt_firstName"];
                users.LastName = collection["txt_lastName"];
                users.UserName = collection["txt_UserName"];
                users.Password = collection["txt_password"];
                users.Email = collection["txt_email"];
                users.Phone = collection["txt_phone"];
                users.RoleId = int.Parse(collection["txt_RoleId"]);
                ViewBag.err = users.Create();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int id)
        {
            List<SelectListItem> RoleLst = new List<SelectListItem>();
            RoleLst.Add(new SelectListItem { Text = "Client", Value = "2" });
            ViewBag.Role = RoleLst;
            ViewBag.Data = users.Read(id).Tables[0].Rows;
            return View();
        }

        // POST: Users/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                users.FirstName = collection["txt_firstName"];
                users.LastName = collection["txt_lastName"];
                users.UserName = collection["txt_UserName"];
                users.Password = collection["txt_password"];
                users.Email = collection["txt_email"];
                users.Phone = collection["txt_phone"];
                users.RoleId = int.Parse(collection["txt_RoleId"]);
                users.ID = id;
                ViewBag.err = users.Update();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int id)
        {
            
            return View();
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                users.ID = id;
                users.Delete();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
