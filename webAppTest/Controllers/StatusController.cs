﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webAppTest.Controllers
{
    public class StatusController : Controller
    {
        private Models.tbl_status status = new Models.tbl_status();
        // GET: status
        public ActionResult Index()
        {
            ViewBag.Data = status.Read().Tables[0];
            return View();
        }

        // GET: status/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: status/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: status/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                status.Name = collection["txt_Name"];
                ViewBag.err = status.Create();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: status/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Data = status.Read(id).Tables[0].Rows;
            return View();
        }

        // POST: status/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                status.Name = collection["txt_Name"];
                status.ID = id;
                ViewBag.err = status.Update();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: status/Delete/5
        public ActionResult Delete(int id)
        {

            return View();
        }

        // POST: status/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                status.ID = id;
                status.Delete();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
