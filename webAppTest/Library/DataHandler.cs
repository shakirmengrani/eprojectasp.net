﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webAppTest.Library
{
    public class DataHandler
    {
        public static T DBValFilter<T>(object val) {
            val = (val == DBNull.Value) ? "" : val;
            return (T)Convert.ChangeType(val, typeof(T));
        }
    }
}